<?php

declare(strict_types=1);

namespace TwentyThree\Doctrine\JsonObjectType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use TwentyThree\Doctrine\JsonObjectType\Exception\DatabaseInvalidDataFormatException;

final class JsonObjectType extends Type
{
    private const INTERNAL_OBJECT_TYPE_NAME = 'json_object';

    private const JSON_ERROR_OFFSET = 10000;

    private const INTERNAL_ERROR_OFFSET = 20000;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * Allows to set the serializer implementation.
     *
     * This method is needed because the abstract doctrine type has final constructor, preventing further
     * dependency injection.
     *
     * @param SerializerInterface|null $serializer
     */
    public function setSerializer(SerializerInterface $serializer = null): void
    {
        $this->serializer = $serializer ?? SerializerBuilder::create()->build();
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_array($value)) {
            return json_encode(
                    array_map(
                    function ($item) use ($platform) {
                        if (null === $item) {
                            return json_decode('{"meta":{"type":"primitive"},"payload":null}');
                        }

                        return json_decode($this->convertToDatabaseValue($item, $platform), true);
                    },
                    $value
                )
            );
        }

        if (\is_object($value)) {
            return sprintf(
                '{"meta":{"type":"object","class":%s},"payload":%s}',
                json_encode(\get_class($value)),
                $this->getSerializer()->serialize($value, 'json')
            );
        }

        if (null === $value) {
            return null;
        }

        return sprintf(
            '{"meta":{"type":"primitive"},"payload":%s}',
            json_encode($value)
        );
    }

    /**
     * {@inheritdoc}
     *
     * @throws DatabaseInvalidDataFormatException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return null;
        }

        $isArray = 0 === mb_strpos($value, '[');

        $value = $this->decodeDatabaseValue($value);

        if ($isArray) {
            return array_map(
                function ($item) use ($platform) {
                    if (null === $item['payload']) {
                        return null;
                    }

                    return $this->convertToPHPValue(json_encode($item), $platform);
                },
                $value
            );
        }

        $this->validateInternalDataStructure($value);

        if ($value['meta']['type'] === 'primitive') {
            return $value['payload'];
        }

        return $this->getSerializer()->deserialize(json_encode($value['payload']), $value['meta']['class'], 'json');
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return static::INTERNAL_OBJECT_TYPE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    private function getSerializer(): SerializerInterface
    {
        if (null === $this->serializer) {
            $this->setSerializer();
        }

        return $this->serializer;
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     *
     * @throws DatabaseInvalidDataFormatException
     */
    private function decodeDatabaseValue($value)
    {
        $value = json_decode($value, true);
        if (null === $value && json_last_error()) {
            throw new DatabaseInvalidDataFormatException(
                sprintf(
                    'The value could not be decoded. See decoding error: %s',
                    json_last_error_msg()
                ),
                static::JSON_ERROR_OFFSET + json_last_error()
            );
        }

        return $value;
    }

    /**
     * @param mixed[] $value
     *
     * @throws DatabaseInvalidDataFormatException
     */
    private function validateInternalDataStructure(array $value): void
    {
        if (!isset($value['meta']['type'])) {
            throw new DatabaseInvalidDataFormatException(
                sprintf(
                    'The value could not be converted from the stored value. See conversion error: %s',
                    'No type provided.'
                ),
                static::INTERNAL_ERROR_OFFSET + 1001
            );
        }
        if (!\in_array($value['meta']['type'], ['primitive', 'object'], true)) {
            throw new DatabaseInvalidDataFormatException(
                sprintf(
                    'The value could not be converted from the stored value. See conversion error: %s',
                    'Invalid type provided. Must be either "primitive" or "object".'
                ),
                static::INTERNAL_ERROR_OFFSET + 1002
            );
        }
        if ($value['meta']['type'] === 'object' && !isset($value['meta']['class'])) {
            throw new DatabaseInvalidDataFormatException(
                sprintf(
                    'The value could not be converted from the stored value. See conversion error: %s',
                    'Missing class name in serialized data.'
                ),
                static::INTERNAL_ERROR_OFFSET + 1003
            );
        }
        if ($value['meta']['type'] === 'object' && !class_exists($value['meta']['class'], true)) {
            throw new DatabaseInvalidDataFormatException(
                sprintf(
                    'The value could not be converted from the stored value. See conversion error: %s',
                    'Provided classname does not exist.'
                ),
                static::INTERNAL_ERROR_OFFSET + 1004
            );
        }
        if (!isset($value['payload'])) {
            throw new DatabaseInvalidDataFormatException(
                sprintf(
                    'The value could not be converted from the stored value. See conversion error: %s',
                    'No payload was provided in data.'
                ),
                static::INTERNAL_ERROR_OFFSET + 1005
            );
        }
    }
}
