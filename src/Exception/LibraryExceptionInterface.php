<?php

declare(strict_types=1);

namespace TwentyThree\Doctrine\JsonObjectType\Exception;

use Throwable;

interface LibraryExceptionInterface extends Throwable
{
}
