<?php

declare(strict_types=1);

namespace TwentyThree\Doctrine\JsonObjectType\Exception;

final class DatabaseInvalidDataFormatException extends RuntimeException
{
}
