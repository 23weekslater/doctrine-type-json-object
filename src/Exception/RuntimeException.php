<?php

declare(strict_types=1);

namespace TwentyThree\Doctrine\JsonObjectType\Exception;

use RuntimeException as PHPRuntimeException;

class RuntimeException extends PHPRuntimeException implements LibraryExceptionInterface
{
}
