23weekslater / doctrine-type-json-object
========================================

This library provides a Doctrine Type implementation for
(de-)serialization of objects from/to JSON using jms/serializer.

How to install?
---------------

This library is framework-agnostic. It only depends on doctrine-dbal.
Nevertheless installation via composer is the only supported way.

First add the library to your composer dependencies. This can either
happen by adding the following code to your composer.json (Please be
aware that released versions are recommended!):

```json
{
  "require": {
    "23weekslater/doctrine-type-json-object": "dev-master"
  }
}
```

Another way to install the library is by using the composer commandline:

```bash
composer require 23weekslater/doctrine-type-json-object
```

How to use?
-----------

This doctrine type can be used in plain doctrine by registering it as
available column type:

```php
use Doctrine\DBAL\Types\Type;
use TwentyThree\Doctrine\JsonObjectType\JsonObjectType;

Type::addType('json_object', JsonObjectType::class);
```

If one is using Symfony Components (prior to Symfony Flex), they must
add it to the doctrine dbal configuration:

```yaml
doctrine:
    dbal:
        types:
            json_object: TwentyThree\Doctrine\JsonObjectType\JsonObjectType
```

Symfony Flex integration will be provided after release of version 1.0.

Contributing
------------



License
-------

This library is subject to be licensed under MIT.
Please see [LICENSE](LICENSE) for details.
