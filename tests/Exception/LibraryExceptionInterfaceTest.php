<?php

declare(strict_types=1);

namespace TwentyThree\Doctrine\JsonObjectType\Exception;

use PHPUnit\Framework\TestCase;

class LibraryExceptionInterfaceTest extends TestCase
{
    private $exceptionsToTest = [
        DatabaseInvalidDataFormatException::class,
        RuntimeException::class,
    ];

    /**
     * This test case ensures that all covered exceptions implement the library exception interface for better exception handling in invoking code.
     *
     * @dataProvider provideExceptionsToTest
     */
    public function testExceptionImplementsLibraryExceptionInterface(string $exceptionClassName): void
    {
        self::assertTrue(is_a($exceptionClassName, LibraryExceptionInterface::class, true));
    }

    /**
     * This data provider provides the exception class names used in other tests.
     */
    public function provideExceptionsToTest(): array
    {
        $data = [];
        foreach ($this->exceptionsToTest as $exceptionToTest) {
            $data[substr(strrchr($exceptionToTest, '\\'), 1)] = [$exceptionToTest];
        }

        return $data;
    }
}
