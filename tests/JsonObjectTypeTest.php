<?php

declare(strict_types=1);

namespace TwentyThree\Doctrine\JsonObjectType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\SqlitePlatform;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use stdClass;
use Throwable;
use TwentyThree\Doctrine\JsonObjectType\Exception\DatabaseInvalidDataFormatException;

class JsonObjectTypeTest extends TestCase
{
    /**
     * @var JsonObjectType
     */
    private $subject;

    /**
     * @var AbstractPlatform
     */
    private $platform;

    /**
     * This test case validates that the type identifier does not change over time.
     */
    public function testGetName(): void
    {
        self::assertEquals('json_object', $this->subject->getName());
    }

    /**
     * This test case ensures that the type always declares to be annotated in sql comment.
     */
    public function testRequiresSQLCommentHint(): void
    {
        self::assertTrue($this->subject->requiresSQLCommentHint($this->platform));
    }

    /**
     * This test case ensures that some sql declaration is given.
     */
    public function testGetSQLDeclaration(): void
    {
        self::assertEquals('CLOB', $this->subject->getSQLDeclaration([], $this->platform));
    }

    /**
     * This test case validates that the conversion from PHP to database storage value works as intended.
     *
     * @dataProvider provideTestData
     *
     * @param mixed       $input
     * @param string|null $output
     */
    public function testConvertToDatabaseValue($input, ?string $output): void
    {
        $actual = $this->subject->convertToDatabaseValue($input, $this->platform);

        if (null !== $input) {
            self::assertNotNull($actual);
            self::assertJson((string) $actual);
        }
        self::assertEquals($output, $actual);
    }

    /**
     * This test case validates that the conversion from database storage value to PHP works as intended.
     *
     * @dataProvider provideTestData
     *
     * @param mixed       $output
     * @param string|null $input
     */
    public function testConvertToPHPValue($output, ?string $input): void
    {
        $actual = $this->subject->convertToPHPValue($input, $this->platform);

        self::assertEquals($output, $actual);
    }

    /**
     * This data provider provides test case data for conversion from PHP to database value and reverse.
     */
    public function provideTestData(): array
    {
        return [
            'null' => [
                /* input */ null,
                /* output */ null,
            ],
            'string' => [
                'Lorem ipsum',
                '{"meta":{"type":"primitive"},"payload":"Lorem ipsum"}',
            ],
            'int' => [
                5,
                '{"meta":{"type":"primitive"},"payload":5}',
            ],
            'bool' => [
                true,
                '{"meta":{"type":"primitive"},"payload":true}',
            ],
            'array of null' => [
                [null, null],
                '[{"meta":{"type":"primitive"},"payload":null},{"meta":{"type":"primitive"},"payload":null}]',
            ],
            'array of string' => [
                ['Lorem', 'ipsum'],
                '[{"meta":{"type":"primitive"},"payload":"Lorem"},{"meta":{"type":"primitive"},"payload":"ipsum"}]',
            ],
            'array of int' => [
                [12, 39],
                '[{"meta":{"type":"primitive"},"payload":12},{"meta":{"type":"primitive"},"payload":39}]',
            ],
            'array of bool' => [
                [true, false],
                '[{"meta":{"type":"primitive"},"payload":true},{"meta":{"type":"primitive"},"payload":false}]',
            ],
            'stdclass' => [
                new stdClass(),
                '{"meta":{"type":"object","class":"stdClass"},"payload":{}}',
            ],
        ];
    }

    /**
     * This test case ensures that proper exceptions are thrown on exceptional input data.
     *
     * @dataProvider provideTestDataForConvertToDatabaseWithExceptions
     *
     * @param mixed $input
     */
    public function testExceptionHandlingConvertToPHPValue($input, array $expectedException): void
    {
        $this->expectException(Throwable::class);

        if (array_key_exists('class', $expectedException)) {
            $this->expectException($expectedException['class']);

            if (array_key_exists('message', $expectedException)) {
                $this->expectExceptionMessage($expectedException['message']);
            }
            if (array_key_exists('message_regexp', $expectedException)) {
                $this->expectExceptionMessageRegExp($expectedException['message_regexp']);
            }
            if (array_key_exists('code', $expectedException)) {
                $this->expectExceptionCode($expectedException['code']);
            }
        }

        $this->subject->convertToPHPValue($input, $this->platform);
    }

    public function provideTestDataForConvertToDatabaseWithExceptions(): array
    {
        return [
            'string, no valid json' => [
                'lorem ipsum',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 10000 + JSON_ERROR_SYNTAX,
                ],
            ],
            'valid json, missing meta' => [
                '{"payload":"lorem"}',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 20000 + 1001,
                ],
            ],
            'valid json, missing type' => [
                '{"meta":{},"payload":null}',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 20000 + 1001,
                ],
            ],
            'valid json, invalid type' => [
                '{"meta":{"type":"foo"},"payload":"lorem"}',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 20000 + 1002,
                ],
            ],
            'valid json, missing class' => [
                '{"meta":{"type":"object"},"payload":{}}',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 20000 + 1003,
                ],
            ],
            'valid json, unknown class' => [
                '{"meta":{"type":"object","class":"PHP_not_existing_class"},"payload":{}}',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 20000 + 1004,
                ],
            ],
            'valid json, class, missing payload' => [
                '{"meta":{"type":"object","class":"stdClass"}}',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 20000 + 1005,
                ],
            ],
            'valid json, primitive, missing payload' => [
                '{"meta":{"type":"primitive"}}',
                [
                    'class' => DatabaseInvalidDataFormatException::class,
                    'code' => 20000 + 1005,
                ],
            ],
        ];
    }

    public function testTypeWithSettingSerializer(): void
    {
        $this->subject->setSerializer(SerializerBuilder::create()->build());

        $actual = $this->subject->convertToDatabaseValue(new stdClass(), $this->platform);

        self::assertJson((string) $actual);
        self::assertEquals('{"meta":{"type":"object","class":"stdClass"},"payload":{}}', $actual);
    }

    protected function setUp(): void
    {
        $this->subject = (new ReflectionClass(JsonObjectType::class))->newInstanceWithoutConstructor();
        $this->platform = new SqlitePlatform();
    }
}
